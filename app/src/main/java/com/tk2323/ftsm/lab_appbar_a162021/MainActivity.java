package com.tk2323.ftsm.lab_appbar_a162021;

import android.content.Intent;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupToolbar();
    }

    private void setupToolbar()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar myActionBar = getSupportActionBar();
        myActionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId())
        {
            case R.id.action_menu_search:
                Intent searchIntent = new Intent(MainActivity.this, SearchActivity.class);
                Toast.makeText(MainActivity.this, "Search is clicked", Toast.LENGTH_SHORT).show();
                startActivity(searchIntent);
                return true;

            case R.id.action_menu_sync:
                Intent syncIntent = new Intent (MainActivity.this, SyncActivity.class);
                Toast.makeText(MainActivity.this, "Sync is clicked", Toast.LENGTH_SHORT).show();
                startActivity(syncIntent);
                return true;

            case R.id.action_menu_settings:
                Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
                Toast.makeText(MainActivity.this, "Settings is clicked", Toast.LENGTH_SHORT).show();
                startActivity(settingsIntent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
